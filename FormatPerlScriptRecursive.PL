#!/usr/bin/env perl
use strict;
use warnings;
use FindBin qw($RealBin);
use Time::HiRes qw(time);
my $actualTimePerlScriptBegan = time();
my $globalModifiedFileCount = 0;
print "\nFORMAT PERL SCRIPT RECURSIVE V01R00 | (C) 2024 JAUZENS | MIT ^_^\n\n";
my $basicExecutionString = "astyle -A1 -s4 -t4 -T4 -xT4 -xU -xt0 -L -m1 -M120 -xg -H -U -xe -y -xb -j -xf -xh --unpad-brackets --squeeze-lines=1 --squeeze-ws --suffix=.ARTISTICSTYLEBACKUP --mode=java --recursive $RealBin\x2F*.PL";
my $capturedOutputString = qx/$basicExecutionString/;
$globalModifiedFileCount = grep(defined, $capturedOutputString =~ m/FORMATTED\x20\x20/gi);
my $emptyString = '';
print $capturedOutputString =~ s/^-+\n.+\n/$emptyString/r =~ s/^-+\n/$emptyString/r =~ s/FORMATTED\x20\x20/FILE\x20WAS\x20RECTIFIED\x20:\x20/gir =~ s/UNCHANGED\x20\x20/FILE\x20WAS\x20IDENTICAL\x20:\x20/gir . "\n";
printf("RETURN EXIT STATUS : %#08X\n", $?);
printf("\n(C) 2024 JAUZENS | MIT ^_^ | FORMAT PERL SCRIPT RECURSIVE V01R00\n\n[+] : %010U = SOURCES\n[+] : %010.4G = SECONDS\n\n", $globalModifiedFileCount, time() - $actualTimePerlScriptBegan);
# COPYRIGHT (C) 2024 JAUZENS | TWITCH.TV/JAUZENS | GITLAB.COM/JAUZENS | MIT ^_^
