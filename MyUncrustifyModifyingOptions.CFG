#
# Code modifying options (non-whitespace)
#

# Add or remove braces on a single-line 'do' statement.
mod_full_brace_do               = force   # ignore/add/remove/force/not_defined

# Add or remove braces on a single-line 'for' statement.
mod_full_brace_for              = force   # ignore/add/remove/force/not_defined

# Add or remove braces on a single-line 'if' statement. Braces will not be
# removed if the braced statement contains an 'else'.
mod_full_brace_if               = force   # ignore/add/remove/force/not_defined

# Whether to enforce that all blocks of an 'if'/'else if'/'else' chain either
# have, or do not have, braces. Overrides mod_full_brace_if.
#
# 0: Don't override mod_full_brace_if
# 1: Add braces to all blocks if any block needs braces and remove braces if
#    they can be removed from all blocks
# 2: Add braces to all blocks if any block already has braces, regardless of
#    whether it needs them
# 3: Add braces to all blocks if any block needs braces and remove braces if
#    they can be removed from all blocks, except if all blocks have braces
#    despite none needing them
mod_full_brace_if_chain         = 2        # unsigned number

# Whether to add braces to all blocks of an 'if'/'else if'/'else' chain.
# If true, mod_full_brace_if_chain will only remove braces from an 'if' that
# does not have an 'else if' or 'else'.
mod_full_brace_if_chain_only    = false    # true/false

# Add or remove braces on single-line 'while' statement.
mod_full_brace_while            = force   # ignore/add/remove/force/not_defined

# Add or remove braces on single-line 'using ()' statement.
mod_full_brace_using            = force   # ignore/add/remove/force/not_defined

# Don't remove braces around statements that span N newlines
mod_full_brace_nl               = 0        # unsigned number

# Whether to prevent removal of braces from 'if'/'for'/'while'/etc. blocks
# which span multiple lines.
#
# Affects:
#   mod_full_brace_for
#   mod_full_brace_if
#   mod_full_brace_if_chain
#   mod_full_brace_if_chain_only
#   mod_full_brace_while
#   mod_full_brace_using
#
# Does not affect:
#   mod_full_brace_do
#   mod_full_brace_function
mod_full_brace_nl_block_rem_mlcond = false    # true/false

# Add or remove unnecessary parentheses on 'return' statement.
mod_paren_on_return             = remove   # ignore/add/remove/force/not_defined

# Add or remove unnecessary parentheses on 'throw' statement.
mod_paren_on_throw              = remove   # ignore/add/remove/force/not_defined

# Whether to fully parenthesize Boolean expressions in 'while' and 'if'
# statement, as in 'if (a && b > c)' => 'if (a && (b > c))'.
mod_full_paren_if_bool          = true    # true/false

# Whether to fully parenthesize Boolean expressions after '='
# statement, as in 'x = a && b > c;' => 'x = (a && (b > c));'.
mod_full_paren_assign_bool      = true    # true/false

# Whether to fully parenthesize Boolean expressions after '='
# statement, as in 'return  a && b > c;' => 'return (a && (b > c));'.
mod_full_paren_return_bool      = true    # true/false

# Whether to remove superfluous semicolons.
mod_remove_extra_semicolon      = true    # true/false

# Whether to remove duplicate include.
mod_remove_duplicate_include    = true    # true/false

# Whether to take care of the case by the mod_sort_xx options.
mod_sort_case_sensitive         = true    # true/false

# Whether to sort consecutive single-line 'import' statements.
mod_sort_import                 = true    # true/false

# (C#) Whether to sort consecutive single-line 'using' statements.
mod_sort_using                  = true    # true/false

# Whether to sort consecutive single-line '#include' statements (C/C++) and
# '#import' statements (Objective-C). Be aware that this has the potential to
# break your code if your includes/imports have ordering dependencies.
mod_sort_include                = true    # true/false

# Whether to prioritize '#include' and '#import' statements that contain
# filename without extension when sorting is enabled.
mod_sort_incl_import_prioritize_filename = true    # true/false

# Whether to prioritize '#include' and '#import' statements that does not
# contain extensions when sorting is enabled.
mod_sort_incl_import_prioritize_extensionless = true    # true/false

# Whether to prioritize '#include' and '#import' statements that contain
# angle over quotes when sorting is enabled.
mod_sort_incl_import_prioritize_angle_over_quotes = true    # true/false

# Whether to ignore file extension in '#include' and '#import' statements
# for sorting comparison.
mod_sort_incl_import_ignore_extension = false    # true/false

# Whether to group '#include' and '#import' statements when sorting is enabled.
mod_sort_incl_import_grouping_enabled = false    # true/false

# Whether to move a 'break' that appears after a fully braced 'case' before
# the close brace, as in 'case X: { ... } break;' => 'case X: { ... break; }'.
mod_move_case_break             = true    # true/false

# Whether to move a 'return' that appears after a fully braced 'case' before
# the close brace, as in 'case X: { ... } return;' => 'case X: { ... return; }'.
mod_move_case_return            = true    # true/false

# Add or remove braces around a fully braced case statement. Will only remove
# braces if there are no variable declarations in the block.
mod_case_brace                  = force   # ignore/add/remove/force/not_defined

# Whether to remove a void 'return;' that appears as the last statement in a
# function.
mod_remove_empty_return         = true    # true/false

# Add or remove the comma after the last value of an enumeration.
mod_enum_last_comma             = force   # ignore/add/remove/force/not_defined

# Syntax to use for infinite loops.
#
# 0: Leave syntax alone (default)
# 1: Rewrite as `for(;;)`
# 2: Rewrite as `while(true)`
# 3: Rewrite as `do`...`while(true);`
# 4: Rewrite as `while(1)`
# 5: Rewrite as `do`...`while(1);`
#
# Infinite loops that do not already match one of these syntaxes are ignored.
# Other options that affect loop formatting will be applied after transforming
# the syntax.
mod_infinite_loop               = 0        # unsigned number

# Add or remove the 'int' keyword in 'int short'.
mod_int_short                   = ignore   # ignore/add/remove/force/not_defined

# Add or remove the 'int' keyword in 'short int'.
mod_short_int                   = ignore   # ignore/add/remove/force/not_defined

# Add or remove the 'int' keyword in 'int long'.
mod_int_long                    = ignore   # ignore/add/remove/force/not_defined

# Add or remove the 'int' keyword in 'long int'.
mod_long_int                    = ignore   # ignore/add/remove/force/not_defined

# Add or remove the 'int' keyword in 'int signed'.
mod_int_signed                  = ignore   # ignore/add/remove/force/not_defined

# Add or remove the 'int' keyword in 'signed int'.
mod_signed_int                  = ignore   # ignore/add/remove/force/not_defined

# Add or remove the 'int' keyword in 'int unsigned'.
mod_int_unsigned                = ignore   # ignore/add/remove/force/not_defined

# Add or remove the 'int' keyword in 'unsigned int'.
mod_unsigned_int                = ignore   # ignore/add/remove/force/not_defined

# If there is a situation where mod_int_* and mod_*_int would result in
# multiple int keywords, whether to keep the rightmost int (the default) or the
# leftmost int.
mod_int_prefer_int_on_left      = false    # true/false